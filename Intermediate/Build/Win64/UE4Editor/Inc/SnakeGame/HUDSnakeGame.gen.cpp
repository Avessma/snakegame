// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/HUDSnakeGame.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHUDSnakeGame() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AHUDSnakeGame_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AHUDSnakeGame();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKEGAME_API UClass* Z_Construct_UClass_UWidgetGameplay_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UWidgetGameOver_NoRegister();
// End Cross Module References
	void AHUDSnakeGame::StaticRegisterNativesAHUDSnakeGame()
	{
	}
	UClass* Z_Construct_UClass_AHUDSnakeGame_NoRegister()
	{
		return AHUDSnakeGame::StaticClass();
	}
	struct Z_Construct_UClass_AHUDSnakeGame_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetGameplayClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WidgetGameplayClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetGameOverClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WidgetGameOverClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetGameplay_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WidgetGameplay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetGameOver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WidgetGameOver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AHUDSnakeGame_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHUDSnakeGame_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "HUDSnakeGame.h" },
		{ "ModuleRelativePath", "HUDSnakeGame.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplayClass_MetaData[] = {
		{ "Category", "HUDSnakeGame" },
		{ "ModuleRelativePath", "HUDSnakeGame.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplayClass = { "WidgetGameplayClass", nullptr, (EPropertyFlags)0x0024080000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AHUDSnakeGame, WidgetGameplayClass), Z_Construct_UClass_UWidgetGameplay_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplayClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplayClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOverClass_MetaData[] = {
		{ "Category", "HUDSnakeGame" },
		{ "ModuleRelativePath", "HUDSnakeGame.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOverClass = { "WidgetGameOverClass", nullptr, (EPropertyFlags)0x0024080000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AHUDSnakeGame, WidgetGameOverClass), Z_Construct_UClass_UWidgetGameOver_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOverClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOverClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplay_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "HUDSnakeGame.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplay = { "WidgetGameplay", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AHUDSnakeGame, WidgetGameplay), Z_Construct_UClass_UWidgetGameplay_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOver_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "HUDSnakeGame.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOver = { "WidgetGameOver", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AHUDSnakeGame, WidgetGameOver), Z_Construct_UClass_UWidgetGameOver_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOver_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOver_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AHUDSnakeGame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplayClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOverClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameplay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHUDSnakeGame_Statics::NewProp_WidgetGameOver,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AHUDSnakeGame_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AHUDSnakeGame>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AHUDSnakeGame_Statics::ClassParams = {
		&AHUDSnakeGame::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AHUDSnakeGame_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AHUDSnakeGame_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AHUDSnakeGame_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AHUDSnakeGame_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AHUDSnakeGame()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AHUDSnakeGame_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AHUDSnakeGame, 2112590872);
	template<> SNAKEGAME_API UClass* StaticClass<AHUDSnakeGame>()
	{
		return AHUDSnakeGame::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AHUDSnakeGame(Z_Construct_UClass_AHUDSnakeGame, &AHUDSnakeGame::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AHUDSnakeGame"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHUDSnakeGame);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
