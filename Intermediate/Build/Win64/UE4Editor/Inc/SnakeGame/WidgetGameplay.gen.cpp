// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/WidgetGameplay.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWidgetGameplay() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_UWidgetGameplay_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UWidgetGameplay();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	UMG_API UClass* Z_Construct_UClass_UTextBlock_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UWidgetGameplay::execUpdateScore)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Score);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateScore(Z_Param_Score);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWidgetGameplay::execSetGameTime)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Seconds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetGameTime(Z_Param_Seconds);
		P_NATIVE_END;
	}
	void UWidgetGameplay::StaticRegisterNativesUWidgetGameplay()
	{
		UClass* Class = UWidgetGameplay::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetGameTime", &UWidgetGameplay::execSetGameTime },
			{ "UpdateScore", &UWidgetGameplay::execUpdateScore },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics
	{
		struct WidgetGameplay_eventSetGameTime_Parms
		{
			float Seconds;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Seconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::NewProp_Seconds = { "Seconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WidgetGameplay_eventSetGameTime_Parms, Seconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::NewProp_Seconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WidgetGameplay.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWidgetGameplay, nullptr, "SetGameTime", nullptr, nullptr, sizeof(WidgetGameplay_eventSetGameTime_Parms), Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWidgetGameplay_SetGameTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWidgetGameplay_SetGameTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics
	{
		struct WidgetGameplay_eventUpdateScore_Parms
		{
			int32 Score;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Score;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::NewProp_Score = { "Score", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WidgetGameplay_eventUpdateScore_Parms, Score), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::NewProp_Score,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WidgetGameplay.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWidgetGameplay, nullptr, "UpdateScore", nullptr, nullptr, sizeof(WidgetGameplay_eventUpdateScore_Parms), Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWidgetGameplay_UpdateScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWidgetGameplay_UpdateScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWidgetGameplay_NoRegister()
	{
		return UWidgetGameplay::StaticClass();
	}
	struct Z_Construct_UClass_UWidgetGameplay_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScoreBlock_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScoreBlock;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeBlock_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TimeBlock;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWidgetGameplay_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWidgetGameplay_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWidgetGameplay_SetGameTime, "SetGameTime" }, // 2685958301
		{ &Z_Construct_UFunction_UWidgetGameplay_UpdateScore, "UpdateScore" }, // 2037391815
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameplay_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WidgetGameplay.h" },
		{ "ModuleRelativePath", "WidgetGameplay.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_ScoreBlock_MetaData[] = {
		{ "BindWidget", "" },
		{ "Comment", "//??????, ??????? ?????? ???????? ???? ? ????? ?? ?????? ? ???????? ???????????? ??????\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WidgetGameplay.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_ScoreBlock = { "ScoreBlock", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWidgetGameplay, ScoreBlock), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_ScoreBlock_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_ScoreBlock_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_TimeBlock_MetaData[] = {
		{ "BindWidget", "" },
		{ "Comment", "// ??????? ????????? ????\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WidgetGameplay.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_TimeBlock = { "TimeBlock", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWidgetGameplay, TimeBlock), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_TimeBlock_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_TimeBlock_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWidgetGameplay_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_ScoreBlock,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWidgetGameplay_Statics::NewProp_TimeBlock,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWidgetGameplay_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWidgetGameplay>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWidgetGameplay_Statics::ClassParams = {
		&UWidgetGameplay::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UWidgetGameplay_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameplay_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWidgetGameplay_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameplay_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWidgetGameplay()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWidgetGameplay_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWidgetGameplay, 2838596155);
	template<> SNAKEGAME_API UClass* StaticClass<UWidgetGameplay>()
	{
		return UWidgetGameplay::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWidgetGameplay(Z_Construct_UClass_UWidgetGameplay, &UWidgetGameplay::StaticClass, TEXT("/Script/SnakeGame"), TEXT("UWidgetGameplay"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWidgetGameplay);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
