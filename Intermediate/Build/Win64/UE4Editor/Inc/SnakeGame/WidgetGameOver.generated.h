// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_WidgetGameOver_generated_h
#error "WidgetGameOver.generated.h already included, missing '#pragma once' in WidgetGameOver.h"
#endif
#define SNAKEGAME_WidgetGameOver_generated_h

#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRestartClicked); \
	DECLARE_FUNCTION(execMenuClicked); \
	DECLARE_FUNCTION(execShowScore);


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRestartClicked); \
	DECLARE_FUNCTION(execMenuClicked); \
	DECLARE_FUNCTION(execShowScore);


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWidgetGameOver(); \
	friend struct Z_Construct_UClass_UWidgetGameOver_Statics; \
public: \
	DECLARE_CLASS(UWidgetGameOver, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(UWidgetGameOver)


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUWidgetGameOver(); \
	friend struct Z_Construct_UClass_UWidgetGameOver_Statics; \
public: \
	DECLARE_CLASS(UWidgetGameOver, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(UWidgetGameOver)


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWidgetGameOver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWidgetGameOver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWidgetGameOver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWidgetGameOver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWidgetGameOver(UWidgetGameOver&&); \
	NO_API UWidgetGameOver(const UWidgetGameOver&); \
public:


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWidgetGameOver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWidgetGameOver(UWidgetGameOver&&); \
	NO_API UWidgetGameOver(const UWidgetGameOver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWidgetGameOver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWidgetGameOver); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWidgetGameOver)


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ScoreBlock() { return STRUCT_OFFSET(UWidgetGameOver, ScoreBlock); } \
	FORCEINLINE static uint32 __PPO__Menu() { return STRUCT_OFFSET(UWidgetGameOver, Menu); } \
	FORCEINLINE static uint32 __PPO__Restart() { return STRUCT_OFFSET(UWidgetGameOver, Restart); } \
	FORCEINLINE static uint32 __PPO__MenuLevel() { return STRUCT_OFFSET(UWidgetGameOver, MenuLevel); } \
	FORCEINLINE static uint32 __PPO__SnakeLevel() { return STRUCT_OFFSET(UWidgetGameOver, SnakeLevel); }


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_16_PROLOG
#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_INCLASS \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WidgetGameOver_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class UWidgetGameOver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_WidgetGameOver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
