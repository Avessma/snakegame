// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_MenuHUD_generated_h
#error "MenuHUD.generated.h already included, missing '#pragma once' in MenuHUD.h"
#endif
#define SNAKEGAME_MenuHUD_generated_h

#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMenuHUD(); \
	friend struct Z_Construct_UClass_AMenuHUD_Statics; \
public: \
	DECLARE_CLASS(AMenuHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AMenuHUD)


#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAMenuHUD(); \
	friend struct Z_Construct_UClass_AMenuHUD_Statics; \
public: \
	DECLARE_CLASS(AMenuHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AMenuHUD)


#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMenuHUD(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMenuHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMenuHUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMenuHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMenuHUD(AMenuHUD&&); \
	NO_API AMenuHUD(const AMenuHUD&); \
public:


#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMenuHUD(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMenuHUD(AMenuHUD&&); \
	NO_API AMenuHUD(const AMenuHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMenuHUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMenuHUD); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMenuHUD)


#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MenuWidgetClass() { return STRUCT_OFFSET(AMenuHUD, MenuWidgetClass); } \
	FORCEINLINE static uint32 __PPO__MenuWidget() { return STRUCT_OFFSET(AMenuHUD, MenuWidget); }


#define SnakeGame_Source_SnakeGame_MenuHUD_h_15_PROLOG
#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_INCLASS \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_MenuHUD_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_MenuHUD_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AMenuHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_MenuHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
