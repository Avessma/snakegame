// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_MenuWidget_generated_h
#error "MenuWidget.generated.h already included, missing '#pragma once' in MenuWidget.h"
#endif
#define SNAKEGAME_MenuWidget_generated_h

#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnQuitGame); \
	DECLARE_FUNCTION(execOnStartGame);


#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnQuitGame); \
	DECLARE_FUNCTION(execOnStartGame);


#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMenuWidget(); \
	friend struct Z_Construct_UClass_UMenuWidget_Statics; \
public: \
	DECLARE_CLASS(UMenuWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(UMenuWidget)


#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMenuWidget(); \
	friend struct Z_Construct_UClass_UMenuWidget_Statics; \
public: \
	DECLARE_CLASS(UMenuWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(UMenuWidget)


#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMenuWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMenuWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMenuWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMenuWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMenuWidget(UMenuWidget&&); \
	NO_API UMenuWidget(const UMenuWidget&); \
public:


#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMenuWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMenuWidget(UMenuWidget&&); \
	NO_API UMenuWidget(const UMenuWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMenuWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMenuWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMenuWidget)


#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StartGame() { return STRUCT_OFFSET(UMenuWidget, StartGame); } \
	FORCEINLINE static uint32 __PPO__QuitGame() { return STRUCT_OFFSET(UMenuWidget, QuitGame); } \
	FORCEINLINE static uint32 __PPO__SnakeLevel() { return STRUCT_OFFSET(UMenuWidget, SnakeLevel); }


#define SnakeGame_Source_SnakeGame_MenuWidget_h_15_PROLOG
#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_INCLASS \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_MenuWidget_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_MenuWidget_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class UMenuWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_MenuWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
