// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_BonusSpeedPlus_generated_h
#error "BonusSpeedPlus.generated.h already included, missing '#pragma once' in BonusSpeedPlus.h"
#endif
#define SNAKEGAME_BonusSpeedPlus_generated_h

#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABonusSpeedPlus(); \
	friend struct Z_Construct_UClass_ABonusSpeedPlus_Statics; \
public: \
	DECLARE_CLASS(ABonusSpeedPlus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusSpeedPlus) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusSpeedPlus*>(this); }


#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_INCLASS \
private: \
	static void StaticRegisterNativesABonusSpeedPlus(); \
	friend struct Z_Construct_UClass_ABonusSpeedPlus_Statics; \
public: \
	DECLARE_CLASS(ABonusSpeedPlus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusSpeedPlus) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusSpeedPlus*>(this); }


#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusSpeedPlus(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABonusSpeedPlus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusSpeedPlus); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusSpeedPlus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusSpeedPlus(ABonusSpeedPlus&&); \
	NO_API ABonusSpeedPlus(const ABonusSpeedPlus&); \
public:


#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusSpeedPlus(ABonusSpeedPlus&&); \
	NO_API ABonusSpeedPlus(const ABonusSpeedPlus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusSpeedPlus); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusSpeedPlus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABonusSpeedPlus)


#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_15_PROLOG
#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_INCLASS \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_BonusSpeedPlus_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ABonusSpeedPlus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_BonusSpeedPlus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
