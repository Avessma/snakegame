// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_WidgetGameplay_generated_h
#error "WidgetGameplay.generated.h already included, missing '#pragma once' in WidgetGameplay.h"
#endif
#define SNAKEGAME_WidgetGameplay_generated_h

#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateScore); \
	DECLARE_FUNCTION(execSetGameTime);


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateScore); \
	DECLARE_FUNCTION(execSetGameTime);


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWidgetGameplay(); \
	friend struct Z_Construct_UClass_UWidgetGameplay_Statics; \
public: \
	DECLARE_CLASS(UWidgetGameplay, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(UWidgetGameplay)


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUWidgetGameplay(); \
	friend struct Z_Construct_UClass_UWidgetGameplay_Statics; \
public: \
	DECLARE_CLASS(UWidgetGameplay, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(UWidgetGameplay)


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWidgetGameplay(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWidgetGameplay) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWidgetGameplay); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWidgetGameplay); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWidgetGameplay(UWidgetGameplay&&); \
	NO_API UWidgetGameplay(const UWidgetGameplay&); \
public:


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWidgetGameplay(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWidgetGameplay(UWidgetGameplay&&); \
	NO_API UWidgetGameplay(const UWidgetGameplay&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWidgetGameplay); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWidgetGameplay); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWidgetGameplay)


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ScoreBlock() { return STRUCT_OFFSET(UWidgetGameplay, ScoreBlock); } \
	FORCEINLINE static uint32 __PPO__TimeBlock() { return STRUCT_OFFSET(UWidgetGameplay, TimeBlock); }


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_15_PROLOG
#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_INCLASS \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WidgetGameplay_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class UWidgetGameplay>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_WidgetGameplay_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
