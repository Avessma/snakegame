// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/WidgetGameOver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWidgetGameOver() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_UWidgetGameOver_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UWidgetGameOver();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	UMG_API UClass* Z_Construct_UClass_UTextBlock_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UButton_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UWidgetGameOver::execRestartClicked)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RestartClicked();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWidgetGameOver::execMenuClicked)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MenuClicked();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWidgetGameOver::execShowScore)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Score);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ShowScore(Z_Param_Score);
		P_NATIVE_END;
	}
	void UWidgetGameOver::StaticRegisterNativesUWidgetGameOver()
	{
		UClass* Class = UWidgetGameOver::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "MenuClicked", &UWidgetGameOver::execMenuClicked },
			{ "RestartClicked", &UWidgetGameOver::execRestartClicked },
			{ "ShowScore", &UWidgetGameOver::execShowScore },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWidgetGameOver_MenuClicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWidgetGameOver_MenuClicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWidgetGameOver_MenuClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWidgetGameOver, nullptr, "MenuClicked", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWidgetGameOver_MenuClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameOver_MenuClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWidgetGameOver_MenuClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWidgetGameOver_MenuClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWidgetGameOver_RestartClicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWidgetGameOver_RestartClicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWidgetGameOver_RestartClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWidgetGameOver, nullptr, "RestartClicked", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWidgetGameOver_RestartClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameOver_RestartClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWidgetGameOver_RestartClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWidgetGameOver_RestartClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics
	{
		struct WidgetGameOver_eventShowScore_Parms
		{
			int32 Score;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Score;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::NewProp_Score = { "Score", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WidgetGameOver_eventShowScore_Parms, Score), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::NewProp_Score,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWidgetGameOver, nullptr, "ShowScore", nullptr, nullptr, sizeof(WidgetGameOver_eventShowScore_Parms), Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWidgetGameOver_ShowScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWidgetGameOver_ShowScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWidgetGameOver_NoRegister()
	{
		return UWidgetGameOver::StaticClass();
	}
	struct Z_Construct_UClass_UWidgetGameOver_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScoreBlock_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScoreBlock;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Menu_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Menu;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Restart_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Restart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MenuLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_MenuLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnakeLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SnakeLevel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWidgetGameOver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWidgetGameOver_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWidgetGameOver_MenuClicked, "MenuClicked" }, // 3349958657
		{ &Z_Construct_UFunction_UWidgetGameOver_RestartClicked, "RestartClicked" }, // 693542462
		{ &Z_Construct_UFunction_UWidgetGameOver_ShowScore, "ShowScore" }, // 448051638
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameOver_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WidgetGameOver.h" },
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_ScoreBlock_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_ScoreBlock = { "ScoreBlock", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWidgetGameOver, ScoreBlock), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_ScoreBlock_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_ScoreBlock_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Menu_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Menu = { "Menu", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWidgetGameOver, Menu), Z_Construct_UClass_UButton_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Menu_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Menu_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Restart_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Restart = { "Restart", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWidgetGameOver, Restart), Z_Construct_UClass_UButton_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Restart_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Restart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_MenuLevel_MetaData[] = {
		{ "Category", "WidgetGameOver" },
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_MenuLevel = { "MenuLevel", nullptr, (EPropertyFlags)0x0044000000010001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWidgetGameOver, MenuLevel), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_MenuLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_MenuLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_SnakeLevel_MetaData[] = {
		{ "Category", "WidgetGameOver" },
		{ "ModuleRelativePath", "WidgetGameOver.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_SnakeLevel = { "SnakeLevel", nullptr, (EPropertyFlags)0x0044000000010001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWidgetGameOver, SnakeLevel), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_SnakeLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_SnakeLevel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWidgetGameOver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_ScoreBlock,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Menu,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_Restart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_MenuLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWidgetGameOver_Statics::NewProp_SnakeLevel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWidgetGameOver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWidgetGameOver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWidgetGameOver_Statics::ClassParams = {
		&UWidgetGameOver::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UWidgetGameOver_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameOver_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWidgetGameOver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWidgetGameOver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWidgetGameOver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWidgetGameOver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWidgetGameOver, 701093555);
	template<> SNAKEGAME_API UClass* StaticClass<UWidgetGameOver>()
	{
		return UWidgetGameOver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWidgetGameOver(Z_Construct_UClass_UWidgetGameOver, &UWidgetGameOver::StaticClass, TEXT("/Script/SnakeGame"), TEXT("UWidgetGameOver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWidgetGameOver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
