// Fill out your copyright notice in the Description page of Project Settings.


#include "HUDSnakeGame.h"
#include "WidgetGameplay.h"
#include "WidgetGameOver.h"

void AHUDSnakeGame::BeginPlay()
{
	Super::BeginPlay();

	WidgetGameplay = CreateWidget<UWidgetGameplay>(GetWorld(), WidgetGameplayClass);
	check(WidgetGameplay);
	WidgetGameplay->AddToViewport();

	WidgetGameOver = CreateWidget<UWidgetGameOver>(GetWorld(), WidgetGameOverClass);
	check(WidgetGameOver);
	WidgetGameOver->AddToViewport();

	WidgetGameOver->SetVisibility(ESlateVisibility::Collapsed);
	WidgetGameplay->SetVisibility(ESlateVisibility::Visible);
}

void AHUDSnakeGame::SetGameTime(float Seconds)
{
	WidgetGameplay->SetGameTime(Seconds);   // ������������� ����� � ������ � ����������� ������� � �������
}

void AHUDSnakeGame::UpdateScore(int32 Score)
{
	WidgetGameplay->UpdateScore(Score);  // ������������� ���� � ������ � ����������� ������� � �������
}

void AHUDSnakeGame::GetScore(int32 Score)
{
	WidgetGameOver->ShowScore(Score);
}

void AHUDSnakeGame::SetGameOver(bool GameOver)
{
	if (GameOver)
	{
		auto* PC = GetWorld()->GetFirstPlayerController();
		PC->SetShowMouseCursor(true);

		WidgetGameOver->SetVisibility(ESlateVisibility::Visible);
		WidgetGameplay->SetVisibility(ESlateVisibility::Collapsed);
	}
}

