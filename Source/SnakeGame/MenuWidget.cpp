// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h" // ��� �������� ������
#include "Kismet/KismetSystemLibrary.h" // ��� �������� ����

void UMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	check(StartGame);
	StartGame->OnClicked.AddDynamic(this, &ThisClass::OnStartGame);

	check(QuitGame);
	QuitGame->OnClicked.AddDynamic(this, &ThisClass::OnQuitGame);
}

void UMenuWidget::OnStartGame()
{
	UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), SnakeLevel);
}

void UMenuWidget::OnQuitGame()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), GetOwningPlayer(), EQuitPreference::Quit, false);
}
