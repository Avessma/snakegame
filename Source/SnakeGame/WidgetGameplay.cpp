// Fill out your copyright notice in the Description page of Project Settings.


#include "WidgetGameplay.h"
#include "Components/TextBlock.h" 
#include "PlayerPawnBase.h"


void UWidgetGameplay::SetGameTime(float Seconds)
{
	if (TimeBlock)
	{
		TimeBlock->SetText(APlayerPawnBase::FormatSeconds(Seconds));  // �������������� �������
	}
}
void UWidgetGameplay::UpdateScore(int32 Score)
{
	ScoreBlock->SetText(APlayerPawnBase::FormatScore(Score));  // �������������� �����
}