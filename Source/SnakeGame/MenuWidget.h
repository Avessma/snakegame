// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidget.generated.h"

/**
 * 
 */

class UButton;

UCLASS()
class SNAKEGAME_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
	class UButton* StartGame;

	UPROPERTY(meta = (BindWidget))
	class UButton* QuitGame;

	UPROPERTY(EditDefaultsOnly)
	TSoftObjectPtr<UWorld> SnakeLevel;

protected:

	virtual void NativeOnInitialized() override;

private:

	UFUNCTION()
	void OnStartGame();

	UFUNCTION()
	void OnQuitGame();
};
