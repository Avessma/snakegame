// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeed.h"
#include "Snake.h"
#include "Engine/Classes/Components/SphereComponent.h"


// Sets default values
ABonusSpeed::ABonusSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SpeedRoot"));
	RootComponent = MyRootComponent;

	SnakeSpeedMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	SpeedColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/Meterials/SpeedMaterial_Inst.SpeedMaterial_Inst'")).Get();

	FVector Size = FVector(0.5f, 0.5f, 0.5f);

	SpeedChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Speed"));

	SpeedChank->SetStaticMesh(SnakeSpeedMesh);
	SpeedChank->SetRelativeScale3D(Size);
	SpeedChank->SetRelativeLocation(FVector(0, 0, 0));
	SpeedChank->SetMaterial(0, SpeedColor);
	SpeedChank->AttachTo(MyRootComponent);
	SpeedChank->SetSimulatePhysics(true);

}

// Called when the game starts or when spawned
void ABonusSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->ChangeSpeed();
			if (Snake->Score != 0)
			{
				Snake->Score = Snake->Score - 35;
			}

			Destroy(true, true);

		}

	}
}


