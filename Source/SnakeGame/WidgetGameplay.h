// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WidgetGameplay.generated.h"

/**
 * 
 */

class UTextBlock;

UCLASS()
class SNAKEGAME_API UWidgetGameplay : public UUserWidget
{
	GENERATED_BODY()

private:

	UPROPERTY(meta = (BindWidget)) //������, ������� ������ �������� ���� � ����� �� ������ � �������� ������������ ������
	class UTextBlock* ScoreBlock;  // ������� ��������� ����

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TimeBlock;


public:

	UFUNCTION()
	void SetGameTime(float Seconds);

	UFUNCTION()
	void UpdateScore(int32 Score);
};
