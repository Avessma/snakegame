// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuHUD.h"
#include "MenuWidget.h"

void AMenuHUD::BeginPlay()
{
	Super::BeginPlay();

	MenuWidget = CreateWidget<UMenuWidget>(GetWorld(), MenuWidgetClass);
	check(MenuWidget);
	MenuWidget->AddToViewport();

	auto* PC = GetWorld()->GetFirstPlayerController();
	PC->SetShowMouseCursor(true);
}
