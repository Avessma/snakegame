// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.3f;
	LastMoveDerection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElements(5);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	GameTime += DeltaTime;

}

void ASnake::AddSnakeElements(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * -ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		NewSnakeElement->SetActorHiddenInGame(true);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
	
}

void ASnake::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	MovementSpeed = ElementSize;
	switch (LastMoveDerection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;

	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;

	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;

	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->SetActorHiddenInGame(false);

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorHiddenInGame(false);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	Moving = false;

	SnakeElements[0]->ToggleCollision();
}

void ASnake::SnakeElementOverlab(ASnakeElementBase* OverlappedElement, AActor* Other)
{
		if (IsValid(OverlappedElement))
		{
			int32 ElemIndex;
			SnakeElements.Find(OverlappedElement, ElemIndex);
			bool bIsFirst = ElemIndex == 0;
			IInteractable* InteractableInterface = Cast<IInteractable>(Other);
			if (InteractableInterface)
			{
				InteractableInterface->Interact(this, bIsFirst);
			}
		}
}

void ASnake::ChangeSpeed()
{
	SetActorTickInterval(0.5f);
	if (!RecoveryTimerHandle.IsValid())
	{
		GetWorldTimerManager().SetTimer(RecoveryTimerHandle, this, &ASnake::BackSpeed, 10.f, true);
	}
}

void ASnake::BackSpeed()
{
	SetActorTickInterval(0.3f);
}

void ASnake::ChangeSpeedPlus()
{
	SetActorTickInterval(0.1f);
	if (!RecoveryTimerHandle.IsValid())
	{
		GetWorldTimerManager().SetTimer(RecoveryTimerHandle, this, &ASnake::BackSpeed, 10.f, true);
	}
}

