// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeedPlus.h"
#include "Snake.h"
#include "Engine/Classes/Components/SphereComponent.h"

// Sets default values
ABonusSpeedPlus::ABonusSpeedPlus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SpeedRoot"));
	RootComponent = MyRootComponent;

	SnakeSpeedPlusMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	SpeedPlusColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/Meterials/SpeedMaterialPlus_Inst.SpeedMaterialPlus_Inst'")).Get();

	FVector Size = FVector(0.5f, 0.5f, 0.5f);

	SpeedPlusChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Speed"));

	SpeedPlusChank->SetStaticMesh(SnakeSpeedPlusMesh);
	SpeedPlusChank->SetRelativeScale3D(Size);
	SpeedPlusChank->SetRelativeLocation(FVector(0, 0, 0));
	SpeedPlusChank->SetMaterial(0, SpeedPlusColor);
	SpeedPlusChank->AttachTo(MyRootComponent);
	SpeedPlusChank->SetSimulatePhysics(true);
}

// Called when the game starts or when spawned
void ABonusSpeedPlus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusSpeedPlus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusSpeedPlus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->ChangeSpeedPlus();
			
			Snake->Score = Snake->Score + 35;

			Destroy(true, true);

		}

	}
}

