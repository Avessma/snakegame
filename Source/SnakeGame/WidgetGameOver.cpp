// Fill out your copyright notice in the Description page of Project Settings.


#include "WidgetGameOver.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

void UWidgetGameOver::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	check(Menu);
	Menu->OnClicked.AddDynamic(this, &ThisClass::MenuClicked);

	check(Restart);
	Restart->OnClicked.AddDynamic(this, &ThisClass::RestartClicked);
}

void UWidgetGameOver::ShowScore(int32 Score)
{
	if (ScoreBlock)
	{
		ScoreBlock->SetText(APlayerPawnBase::FormatScore(Score));
	}
}

void UWidgetGameOver::MenuClicked()
{
	UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), MenuLevel);
}

void UWidgetGameOver::RestartClicked()
{
	UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), SnakeLevel);
}