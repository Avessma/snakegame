// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Snake.h"
#include "Engine/Classes/Components/SphereComponent.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootFood"));
	RootComponent = MyRootComponent;

	SnakeFoodMash = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/Meterials/apple_nurbsToPoly1.apple_nurbsToPoly1'")).Object;
	
	FoodColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/Meterials/AppleMaterial_Inst.AppleMaterial_Inst'")).Get();

	FVector Size = FVector(0.8f, 0.8f, 0.8f);

	FoodChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food"));

	FoodChank->SetStaticMesh(SnakeFoodMash);
	FoodChank->SetRelativeScale3D(Size);
	FoodChank->SetRelativeLocation(FVector(0, 0, 0));
	FoodChank->SetMaterial(0, FoodColor);
	FoodChank->AttachTo(MyRootComponent);
	FoodChank->SetSimulatePhysics(true);

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElements();
			Snake->Score = Snake->Score + 50;
			Destroy(true, true);
		}
	}
}

