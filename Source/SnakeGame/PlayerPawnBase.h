// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnake;
class AHUDSnakeGame;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnake* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnake> SnakeActorClass;

	UPROPERTY()
	AHUDSnakeGame* HUD;

	float XMax = 400.f; float XMin = -400.f;

	float YMax = 900.f; float YMin = -900.f;

	float FoodSpawnZ = 50.f;

	float StepDelayFood = 2.f; float StepDelaySpeed = 20.f; float StepDelaySpeedPlus = 15.f;

	float BuferTimeFood = 0; float BuferTimeSpeed = 0; float BuferTimeSpeedPlus = 0;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();
	void CreateWidgetGameplay();

	UFUNCTION()
	void HandlerPlayerHorizontalInput(float value);

	UFUNCTION()
	void HandlerPlayerVerticalInput(float value);

	UFUNCTION()
	void SpawnBonus();

	UFUNCTION()
	void SpawnSpeed();

	UFUNCTION()
	void SpawnSpeedPlus();

	static FText FormatSeconds(float TimeSeconds);
		
	static FText FormatScore(int32 Score);

};
