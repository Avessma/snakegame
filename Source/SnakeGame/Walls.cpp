// Fill out your copyright notice in the Description page of Project Settings.


#include "Walls.h"
#include "Snake.h"

// Sets default values
AWalls::AWalls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//SnakeWallMash = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("MaterialInstanceConstant'/Engine/StarterContent/Textures/T_ground_Moss_Mat_Inst.T_ground_Moss_Mat_Inst'")).Object;

	//WallChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	//WallChank->SetStaticMesh(SnakeWallMash);
}

// Called when the game starts or when spawned
void AWalls::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AWalls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWalls::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
			Snake->GameOver = true;
			UE_LOG(LogTemp, Warning, TEXT("---------- Game Over ----------"));
			UE_LOG(LogTemp, Warning, TEXT("GameOver: '%d'"), Snake->GameOver);
		}

	}
}
