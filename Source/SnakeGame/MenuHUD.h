// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MenuHUD.generated.h"

/**
 * 
 */

class UMenuWidget;

UCLASS()
class SNAKEGAME_API AMenuHUD : public AHUD
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UMenuWidget> MenuWidgetClass;

	UPROPERTY()
	UMenuWidget* MenuWidget;

	virtual void BeginPlay() override;
};
