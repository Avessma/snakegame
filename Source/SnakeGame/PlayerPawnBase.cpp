// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Snake.h"
#include "Food.h"
#include "BonusSpeed.h"
#include "BonusSpeedPlus.h"
#include "HUDSnakeGame.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();

	auto* PC = GetWorld()->GetFirstPlayerController();
	HUD = Cast<AHUDSnakeGame>(PC->GetHUD());

	if (IsValid(SnakeActor))
	{
		PC->SetShowMouseCursor(false);
		PC->SetInputMode(FInputModeGameOnly());
	}
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(SnakeActor))
	{
		HUD->SetGameTime(SnakeActor->GetGameTime()); // ���������� Time ������ ���; ���������� ������� ����
		HUD->UpdateScore(SnakeActor->GetScore());  // ���������� Score ������ ���
	}

	if (SnakeActor->GameOver)
	{
		
		HUD->SetGameOver(SnakeActor->GetGameOver());
		HUD->GetScore(SnakeActor->GetScore());
	}
	
	BuferTimeFood += DeltaTime;
	if (BuferTimeFood > StepDelayFood)
	{
		SpawnBonus();
		BuferTimeFood = 0;
	}

	BuferTimeSpeed += DeltaTime;
	if (BuferTimeSpeed > StepDelaySpeed)
	{
		SpawnSpeed();
		BuferTimeSpeed = 0;
	}

	BuferTimeSpeedPlus += DeltaTime;
	if (BuferTimeSpeedPlus > StepDelaySpeedPlus)
	{
		SpawnSpeedPlus();
		BuferTimeSpeedPlus = 0;
	}

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);


	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlerPlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlerPlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor() {
	SnakeActor = GetWorld()->SpawnActor<ASnake>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlerPlayerVerticalInput(float value)
{
	//UE_LOG(LogTemp, Warning, TEXT("Vertical Input: %f"), value)
	
		if (IsValid(SnakeActor) && !SnakeActor->Moving)
		{
			if (value > 0 && SnakeActor->LastMoveDerection != EMovementDirection::DOWN)
			{
				SnakeActor->LastMoveDerection = EMovementDirection::UP;
				SnakeActor->Moving = true;
			}
			else if (value < 0 && SnakeActor->LastMoveDerection != EMovementDirection::UP)
			{
				SnakeActor->LastMoveDerection = EMovementDirection::DOWN;
				SnakeActor->Moving = true;
			}
		}
	
}

void APlayerPawnBase::SpawnBonus()
{
	FRotator FoodSpawnRotation = FRotator(0, 0, 0);

	float FoodSpawnX = FMath::FRandRange(XMin, XMax);
	float FoodSpawnY = FMath::FRandRange(YMin, YMax);

	FVector FoodSpawnLocation = FVector(FoodSpawnX, FoodSpawnY, FoodSpawnZ);

	if (IsValid(SnakeActor))
	{
		if (GetWorld())
		{
			GetWorld()->SpawnActor<AFood>(FoodSpawnLocation, FoodSpawnRotation);
		}
	}
}

void APlayerPawnBase::SpawnSpeed()
{
	FRotator FoodSpawnRotation = FRotator(0, 0, 0);

	float FoodSpawnX = FMath::FRandRange(XMin, XMax);
	float FoodSpawnY = FMath::FRandRange(YMin, YMax);

	FVector FoodSpawnLocation = FVector(FoodSpawnX, FoodSpawnY, FoodSpawnZ);

	if (IsValid(SnakeActor))
	{
		if (GetWorld())
		{
			GetWorld()->SpawnActor<ABonusSpeed>(FoodSpawnLocation, FoodSpawnRotation);
		}
	}
}

void APlayerPawnBase::SpawnSpeedPlus()
{
	FRotator FoodSpawnRotation = FRotator(0, 0, 0);

	float FoodSpawnX = FMath::FRandRange(XMin, XMax);
	float FoodSpawnY = FMath::FRandRange(YMin, YMax);

	FVector FoodSpawnLocation = FVector(FoodSpawnX, FoodSpawnY, FoodSpawnZ);

	if (IsValid(SnakeActor))
	{
		if (GetWorld())
		{
			GetWorld()->SpawnActor<ABonusSpeedPlus>(FoodSpawnLocation, FoodSpawnRotation);
		}
	}
}

FText APlayerPawnBase::FormatSeconds(float TimeSeconds)
{
	const int32 TotalSeconds = FMath::RoundToInt(TimeSeconds);
	const int32 Minutes = TotalSeconds / 60;  // ���������� ���������� �����
	const int32 Seconds = TotalSeconds % 60; // ������� �� ���� �� 60, ������� ����� ����� ���-�� ������ �� ������ 60
	const FString FormattedTime = FString::Printf(TEXT("%02i:%02i"), Minutes, Seconds);  // ������� ��� ����� (������:�������) �� �������������� ������� %i, � ���� ��� � ��� ������� �� ������ �����, ����������� ����
	return FText::FromString(FormattedTime);    // �������� ������� ����������� � FText
}

FText APlayerPawnBase::FormatScore(int32 Score)
{
	const FString FormattedScore = FString::Printf(TEXT("%04i"), Score);  // ����� ����� � ����������� ������, ���� ���-�� ����� ������ 1000
	return FText::FromString(FormattedScore);   // �������� ������� ����������� � FText
}

void APlayerPawnBase::HandlerPlayerHorizontalInput(float value)
{	
		if (IsValid(SnakeActor) && SnakeActor->Moving == false)
		{
			if (value > 0 && SnakeActor->LastMoveDerection != EMovementDirection::LEFT)
			{
				SnakeActor->LastMoveDerection = EMovementDirection::RIGHT;
				SnakeActor->Moving = true;
			}
			else if (value < 0 && SnakeActor->LastMoveDerection != EMovementDirection::RIGHT)
			{
				SnakeActor->LastMoveDerection = EMovementDirection::LEFT;
				SnakeActor->Moving = true;
			}
		}
	
}

