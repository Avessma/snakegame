// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WidgetGameOver.generated.h"

/**
 * 
 */

class UTextBlock;
class UButton;

UCLASS()
class SNAKEGAME_API UWidgetGameOver : public UUserWidget
{
	GENERATED_BODY()
protected:

	virtual void NativeOnInitialized() override;

private:

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ScoreBlock;

	UPROPERTY(meta = (BindWidget))
	class UButton* Menu;

	UPROPERTY(meta = (BindWidget))
	class UButton* Restart;

	UPROPERTY(EditDefaultsOnly)
	TSoftObjectPtr<UWorld> MenuLevel;

	UPROPERTY(EditDefaultsOnly)
	TSoftObjectPtr<UWorld> SnakeLevel;

public:

	UFUNCTION()
	void ShowScore(int32 Score);

	UFUNCTION()
	void MenuClicked();

	UFUNCTION()
	void RestartClicked();
};
