// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDerection;

	UPROPERTY()
	bool Moving;

	UPROPERTY()
	FTimerHandle RecoveryTimerHandle;

	UPROPERTY()
	bool GameOver{ false };

	UPROPERTY()
	int32 Score{0};

	UPROPERTY()
	float GameTime{0.0f};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElements(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlab(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION()
	void ChangeSpeed();

	UFUNCTION()
	void BackSpeed();

	UFUNCTION()
	void ChangeSpeedPlus();

	float GetGameTime() const { return GameTime; }; // �������, ������� ���������� GameTime

	int32 GetScore() const { return Score; }; // �������, ������� ���������� Score

	bool GetGameOver() const { return GameOver; };

};
