// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BonusSpeedPlus.generated.h"

class USphereComponent;
class UStaticMesh;
class UStaticMeshComponent;
class UMaterialInstance;

UCLASS()
class SNAKEGAME_API ABonusSpeedPlus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusSpeedPlus();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USphereComponent* MyRootComponent;

	class UStaticMesh* SnakeSpeedPlusMesh;
	class UStaticMeshComponent* SpeedPlusChank;
	class UMaterialInstance* SpeedPlusColor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
