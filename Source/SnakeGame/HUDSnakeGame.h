// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "HUDSnakeGame.generated.h"

/**
 * 
 */
class UWidgetGameplay;
class UWidgetGameOver;

UCLASS()
class SNAKEGAME_API AHUDSnakeGame : public AHUD
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWidgetGameplay> WidgetGameplayClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWidgetGameOver> WidgetGameOverClass;

	UPROPERTY()
	UWidgetGameplay* WidgetGameplay;

	UPROPERTY()
	UWidgetGameOver* WidgetGameOver;

	virtual void BeginPlay() override;

public:
	void SetGameTime(float Seconds);
	void UpdateScore(int32 Score);
	void GetScore(int32 Score);
	void SetGameOver(bool GameOver);
	
};
